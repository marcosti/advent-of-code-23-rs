use std::{
    error::Error,
    fs,
    process, collections::HashMap
};

enum Part {
    PartOne,
    PartTwo
}

fn get_number(line: String) -> Result<u32, &'static str> {
    let mut ret = String::new();
    for c in line.chars() {
        if c.is_numeric() {
            ret.push(c);
            break;
        }
    }
    for c in line.chars().rev() {
        if c.is_numeric() {
            ret.push(c);
            break;
        }
    }
    let ret = ret.parse::<u32>();
    if let Ok(val) = ret {
        Ok(val)
    } else {
        Err("Could not parse u32 from string")
    }
}

fn string_to_digit(s: &str) -> Option<u32> {
    match s {
        "zero" => Some(0),
        "one" => Some(1),
        "two" => Some(2),
        "three" => Some(3),
        "four" => Some(4),
        "five" => Some(5),
        "six" => Some(6),
        "seven" => Some(7),
        "eight" => Some(8),
        "nine" => Some(9),
        _ => None
    }
}

fn get_number_v2(line: String) -> Result<u32, &'static str> {
    let mut ret: u32 = 0;
    let mut findex = line.len();
    let mut map: HashMap<String, u32> = HashMap::new();
    map.insert("zero".to_string(), 0);
    map.insert("0".to_string(), 0);
    map.insert("one".to_string(), 1);
    map.insert("1".to_string(), 1);
    map.insert("two".to_string(), 2);
    map.insert("2".to_string(), 2);
    map.insert("three".to_string(), 3);
    map.insert("3".to_string(), 3);
    map.insert("four".to_string(), 4);
    map.insert("4".to_string(), 4);
    map.insert("five".to_string(), 5);
    map.insert("5".to_string(), 5);
    map.insert("six".to_string(), 6);
    map.insert("6".to_string(), 6);
    map.insert("seven".to_string(), 7);
    map.insert("7".to_string(), 7);
    map.insert("eight".to_string(), 8);
    map.insert("8".to_string(), 8);
    map.insert("nine".to_string(), 9);
    map.insert("9".to_string(), 9);

    let mut res = Err("Could not parse u32 from string");
    for key in map.keys() {
        if let Some(index) = line.find(key) {
            if index < findex {
                findex = index;
                res = Ok(map.get(key).unwrap());
            }
        }
    }

    if let Err(e) = res {
        return Err(e);
    }
    if let Ok(val) = res {
        ret = val * 10;
    }

    findex = 0;

    for key in map.keys() {
        if let Some(index) = line.rfind(key) {
            if index > findex {
                findex = index;
                res = Ok(map.get(key).unwrap());
            }
        }
    }
    if let Ok(val) = res {
        ret += val;
    }

    Ok(ret)
}

fn check_file(path: &str, part: Part) -> Result<Vec<u32>, Box<dyn Error>> {
    let mut ret: Vec<u32> = vec![];
    let contents = fs::read_to_string(path)?;
    for line in contents.lines() {
        if let Ok(val) = match part {
            Part::PartOne => get_number(line.to_string()),
            Part::PartTwo => get_number_v2(line.to_string())
        }
        {
            ret.push(val);
        }
    }

    Ok(ret)
}

fn main() {
    let sum = check_file("./assets/input", Part::PartOne).unwrap_or_else(|err| {
        eprintln!("Error while reading the file: {err}");
        process::exit(1);
    });
    let sum: u32 = sum.iter().sum();
    println!("1) Sum is: {sum}");
    let sum = check_file("./assets/input", Part::PartTwo).unwrap_or_else(|err| {
        eprintln!("Error while reading the file: {err}");
        process::exit(1);
    });
    let sum: u32 = sum.iter().sum();
    println!("2) Sum is: {sum}");
}
