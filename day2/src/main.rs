use std::{
    self, error::Error, fs, process
};

const MAX_R: u32 = 12;
const MAX_G: u32 = 13;
const MAX_B: u32 = 14;

struct RGB {
    r: u32,
    g: u32,
    b: u32
}

struct Game {
    results: Vec<RGB>,
    game_number: u32,
    minimum_cubes: RGB
}

impl Game {
    fn new(n: u32) -> Self {
        Game { results: vec![], game_number: n, minimum_cubes: RGB { r: 0, g: 0, b: 0 } }
    }
    fn is_possible(&self) -> bool {
        if self.results.len() == 0 {
            return false;
        }

        for res in &self.results {
            if res.r > MAX_R || res.g > MAX_G || res.b > MAX_B {
                return false;
            }
        }
        true
    }
    fn _update_minimum_cubes(&mut self) -> Option<()> {
        let last_game = self.results.last()?;
        self.minimum_cubes.r = if last_game.r > self.minimum_cubes.r { last_game.r } else { self.minimum_cubes.r };
        self.minimum_cubes.g = if last_game.g > self.minimum_cubes.g { last_game.g } else { self.minimum_cubes.g };
        self.minimum_cubes.b = if last_game.b > self.minimum_cubes.b { last_game.b } else { self.minimum_cubes.b };
        Some(())
    }
    fn push_result(&mut self, result: &str) -> Result<(), Box<dyn Error>> {
        let mut r: u32 = 0;
        let mut g: u32 = 0;
        let mut b: u32 = 0;

        for field in result.split(",") {
            if let Some((num, _)) = field.split_once(" red") {
                r = num.trim().parse::<u32>()?;
            }
            if let Some((num, _)) = field.split_once(" green") {
                g = num.trim().parse::<u32>()?;
            }
            if let Some((num, _)) = field.split_once(" blue") {
                b = num.trim().parse::<u32>()?;
            }
        }

        self.results.push(RGB { r, g, b });
        self._update_minimum_cubes();

        Ok(())
    }
    fn game_power(&self) -> u32 {
        &self.minimum_cubes.r * &self.minimum_cubes.g * &self.minimum_cubes.b
    }
}

fn sum_of_possible_games(path: &str) -> Result<u32, Box<dyn Error>> {
    let file = fs::read_to_string(path)?;
    let mut sum: u32 = 0;
    for line in file.lines() {
        let splits = line.split_once(":");
        if let Some((game, results)) = splits {
            let mut game = Game::new(game.split_once("Game ").unwrap().1.parse::<u32>()?);
            let results: Vec<&str> = results.split(";").collect();
            for res in results {
                if let Err(e) = game.push_result(res) {
                    eprintln!("Invalid result '{res}': {e}");
                    continue;
                }
            }
            if game.is_possible() {
                sum += game.game_number;
            }
        } else {
            eprint!("Invalid game: {:?}", splits);
            continue;
        }
    }

    Ok(sum)
}

fn sum_of_game_powers(path: &str) -> Result<u32, Box<dyn Error>> {
    let file = fs::read_to_string(path)?;
    let mut sum: u32 = 0;
    for line in file.lines() {
        let splits = line.split_once(":");
        if let Some((game, results)) = splits {
            let mut game = Game::new(game.split_once("Game ").unwrap().1.parse::<u32>()?);
            let results: Vec<&str> = results.split(";").collect();
            for res in results {
                if let Err(e) = game.push_result(res) {
                    eprintln!("Invalid result '{res}': {e}");
                    continue;
                }
            }
            sum += game.game_power();
        } else {
            eprint!("Invalid game: {:?}", splits);
            continue;
        }
    }

    Ok(sum)
}

fn main() {
    let result = sum_of_possible_games("./assets/input");
    if let Err(e) = result {
        eprint!("Error while processing input: {e}");
        process::exit(1);
    }
    println!("Sum is: {}", result.unwrap());
    let result = sum_of_game_powers("./assets/input");
    if let Err(e) = result {
        eprint!("Error while processing input: {e}");
        process::exit(1);
    }
    println!("Sum is: {}", result.unwrap());
}
