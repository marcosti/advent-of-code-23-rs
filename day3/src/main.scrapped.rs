use std::{
    fs, self, error::Error, process
};

fn is_symbol(c: char) -> bool {
    c != '.' && !c.is_alphanumeric()
}

fn get_number(line: &mut String, j: usize) -> Result<u32, Box<dyn Error>> {
    let mut cnt: usize = 0;
    let mut num = String::new();

    let ch = line.chars().nth(j).unwrap();
    if ch.is_numeric() {
        num.push(ch)
    }
    while j+cnt+1 < line.len() && line.chars().nth(j+cnt+1).unwrap().is_numeric() {
        num.push(line.chars().nth(j+cnt+1).unwrap());
        line.remove(j+cnt+1);
        line.insert(j+cnt+1, '.');
        cnt += 1;
    };
    let num = num.parse::<u32>()?;
    Ok(num)
}

fn get_number_backwards(line: &mut String, j: usize) -> Result<u32, Box<dyn Error>> {
    let mut cnt: usize = 0;
    let mut num = String::new();

    let ch = line.chars().nth(j).unwrap();
    if ch.is_numeric() {
        num.push(ch)
    }
    while j-cnt-1 > 0 && line.chars().nth(j-cnt-1).unwrap().is_numeric() {
        num.push(line.chars().nth(j-cnt-1).unwrap());
        line.remove(j-cnt-1);
        line.insert(j-cnt-1, '.');
        cnt += 1;
    };
    num = num.chars().rev().collect();
    let num = num.parse::<u32>()?;
    Ok(num)
}

fn get_number_bi(line: &mut String, j: usize) -> Result<u32, Box<dyn Error>> {
    let mut cnt: usize = 0;
    let mut num = String::new();

    num.push(line.chars().nth(j).unwrap());
    line.remove(j);
    line.insert(j, '.');
    while j+cnt+1 < line.len() && line.chars().nth(j+cnt+1).unwrap().is_numeric() {
        num.push(line.chars().nth(j+cnt+1).unwrap());
        line.remove(j+cnt+1);
        line.insert(j+cnt+1, '.');
        cnt += 1;
    };
    num = num.chars().rev().collect();
    cnt = 0;
    while j-cnt-1 < line.len() && line.chars().nth(j-cnt-1).unwrap().is_numeric() {
        num.push(line.chars().nth(j-cnt-1).unwrap());
        line.remove(j-cnt-1);
        line.insert(j-cnt-1, '.');
        cnt += 1;
    };
    num = num.chars().rev().collect();
    let num = num.parse::<u32>()?;
    Ok(num)
}

fn get_adjacent_numbers(file: &mut Vec<String>, i: usize, j: usize) -> Vec<u32> {
    let mut ret: Vec<u32> = vec![];

    for l in 0..2 {
        if i+l-1 < 0 || i+l-1 > file.len() {
            continue;
        }
        let line = &mut file[i+l-1];

        if line.chars().nth(j).unwrap().is_numeric() {
            if line.chars().nth(j-1).unwrap().is_numeric() && line.chars().nth(j+1).unwrap().is_numeric() {
                let res = get_number_bi(line, j);
                if let Err(e) = res {
                    eprint!("Parsing error: {e}")
                } else {
                    ret.push(res.unwrap());
                }
            }
        } else {
            if line.chars().nth(j+1).unwrap().is_numeric() {
                let res = get_number(line, j);
                if let Err(e) = res {
                    eprint!("Parsing error: {e}")
                } else {
                    ret.push(res.unwrap());
                }
            }
            if line.chars().nth(j-1).unwrap().is_numeric() {
                let res = get_number_backwards(line, j);
                if let Err(e) = res {
                    eprint!("Parsing error: {e}")
                } else {
                    ret.push(res.unwrap());
                }
            }
        }
    }

    ret
}

fn read_file(path: &str) -> Result<u32, Box<dyn Error>> {
    let file = fs::read_to_string(path)?;
    let mut file: Vec<String> = file.lines().map(|s| s.to_owned()).collect();

    let mut sum: u32 = 0;

    for j in 0..file.len() {
        let cur: String = file[j].clone();

        for i in 0..cur.len() {
            let c = cur.chars().nth(i).unwrap();
            print!("{c}");
            if is_symbol(c) {
                sum += get_adjacent_numbers(&mut file, i, j).iter().sum::<u32>();
            } else {
                continue;
            }
        }
        println!();
    }

    Ok(sum)
}

fn main() {
    let res = read_file("./assets/input");
    if let Err(e) = res {
        eprint!("Error reading the file: {e}");
        process::exit(1);
    } else {
        println!("Sum is: {}", res.unwrap());
    }
}
