use std::{
    fs, self, error::Error, process, usize
};
use colored::Colorize;

fn is_symbol(c: char) -> bool {
    c != '.' && c != '~' && !c.is_alphanumeric()
}

fn has_adjacent_symbols(line: &mut Vec<String>, i: usize, j: usize, len: usize) -> bool {
    // println!("{i}, {j}, {len}");
    let prev: Option<&str> = if i > 0 {Some(&line[i-1][if j > 0 {j-1} else {0}..=if j+len < line[i-1].len()-1 {j+len} else {line[i-1].len()-1}])} else {None};
    if let Some(prev) = prev {
        for c in prev.chars() {
            if is_symbol(c) {
                return true;
            }
        }
    }
    let next: Option<&str> = if i < line.len()-1 {Some(&line[i+1][if j > 0 {j-1} else {0}..=if j+len < line[i+1].len()-1 {j+len} else {line[i+1].len()-1}])} else {None};
    if let Some(next) = next {
        for c in next.chars() {
            if is_symbol(c) {
                return true;
            }
        }
    }
    if j > 0 && is_symbol(line[i].chars().nth(j-1).unwrap()) {
        return true;
    }
    if j+len <= line[0].len()-1 && is_symbol(line[i].chars().nth(j+len).unwrap()) {
        return true;
    }
    false
}

fn has_2_adjacent_numbers(line: &mut Vec<String>, i: usize, j: usize) -> Option<((usize, usize), (usize, usize))> {
    // println!("{i}, {j}, {len}");
    let len = 1;
    let mut ret: Vec<(usize, usize)> = vec![];
    if i > 0 {
        for J in if j > 0 {j-1} else {0}..=if j+len < line[i-1].len()-1 {j+len} else {line[i-1].len()-1} {
            if line[i-1].chars().nth(J).unwrap().is_numeric() {
                ret.push((i-1, J));
            }
        }
    };
    if i < line.len()-1 {
        for J in if j > 0 {j-1} else {0}..=if j+len < line[i+1].len()-1 {j+len} else {line[i+1].len()-1} {
            if line[i+1].chars().nth(J).unwrap().is_numeric() {
                ret.push((i+1, J));
            }
        }
    };
    if j > 0 && line[i].chars().nth(j-1).unwrap().is_numeric() {
        ret.push((i, j-1));
    }
    if j+len <= line[0].len()-1 && line[i].chars().nth(j+len).unwrap().is_numeric() {
        ret.push((i, j+1));
    }

    if ret.len() != 2 {
        return None;
    }

    Some((ret[1], ret[2]))
}

fn read_file(path: &str) -> Result<u32, Box<dyn Error>> {
    let file = fs::read_to_string(path)?;
    let mut curnum;
    let mut file: Vec<String> = file.lines().map(|s| s.to_owned()).collect();

    let mut sum: u32 = 0;

    for i in 0..file.len() {
        let mut cur: String = file[i].clone();

        for j in 0..cur.len() {
            curnum = String::new();
            let c = cur.chars().nth(j).unwrap();
            if c.is_numeric() {
                let mut cnt: usize = 0;
                while j+cnt < cur.len() && cur.chars().nth(j+cnt).unwrap().is_numeric() {
                    curnum.push(cur.chars().nth(j+cnt).unwrap());
                    cur.remove(j+cnt);
                    cur.insert(j+cnt, '~');
                    cnt += 1;
                }
            } else {
                if c != '~' {
                    print!("{c}");
                }
                continue;
            }
            if has_adjacent_symbols(&mut file, i, j, curnum.len()) {
                print!("{}", curnum.bold().red());
                sum += curnum.parse::<u32>()?;
            } else {
                print!("{curnum}");
                continue;
            }
        }
        println!();
    }

    Ok(sum)
}

fn read_file_gear(path: &str) -> Result<u32, Box<dyn Error>> {
    let file = fs::read_to_string(path)?;
    let mut file: Vec<String> = file.lines().map(|s| s.to_owned()).collect();

    let mut sum: u32 = 0;

    for i in 0..file.len() {
        let mut cur: String = file[i].clone();

        for j in 0..cur.len() {
            let c = cur.chars().nth(j).unwrap();
            if c != '*' {
                // print!("{c}");
                continue;
            }
            let result = has_2_adjacent_numbers(&mut file, i, j);
            if let Some(result) = result {
                // print!("{}", curnum.bold().red());
                // sum += curnum.parse::<u32>()?;
            } else {
                // print!("{curnum}");
                continue;
            }
        }
        println!();
    }

    Ok(sum)
}

fn main() {
    let res = read_file("./assets/input");
    if let Err(e) = res {
        eprint!("Error reading the file: {e}");
        process::exit(1);
    } else {
        println!("Sum is: {}", res.unwrap());
    }
    let res = read_file("./assets/input");
    if let Err(e) = res {
        eprint!("Error reading the file: {e}");
        process::exit(1);
    } else {
        println!("Sum is: {}", res.unwrap());
    }
}
